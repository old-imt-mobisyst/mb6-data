# ROS Laser data sets

### ROS Bag files

- `tbot_bag_second_loop.bag` = Ceri SN 1st floor including loops

```
rosbag info tbot_bag_second_loop.bag
version:     2.0
duration:    14:16s (856s)
start:       Nov 20 2015 19:19:32.30 (1448043572.30)
end:         Nov 20 2015 19:33:48.77 (1448044428.77)
size:        86.2 MB
messages:    158229
compression: none [113/113 chunks]
types:       nav_msgs/Odometry     [cd5e73d190d741a2f92e81eda573aca7]
             sensor_msgs/LaserScan [90c7ef2dc6895d81024acba2ac42f369]
             tf2_msgs/TFMessage    [94810edda583a504dfda3829e70d7eec]
topics:      /tf    106933 msgs    : tf2_msgs/TFMessage    (3 connections)
             odom    42767 msgs    : nav_msgs/Odometry    
             scan     8529 msgs    : sensor_msgs/LaserScan
```

### How to

```
roslaunch tbot_bags playBag.launch
```

### Aditionnal bags

- http://car.imt-lille-douai.fr/polyslam/


